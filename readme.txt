Bratty JS Includes v0.0.5
    Allows you to include data from other URLs simply using only JavaScript.  The aim is to approximate a Server Side Include using JS and iFrames rather than AJAX.  This allows you to use the includes on localhost and for cross-domain inclusion.

USAGE:
    To use Bratty JS Includes, you must include "bratty.js" on any page that will include or be included.
    The 'data-include' attributes can be used on ANY element, not just anchors.

EXAMPLES:
    <a href='#' data-include='path/to/file.html'></a> <!-- will APPEND elements -->
    <a href='#' data-include='path/to/file.html' data-include-type='prepend'></a> <!-- will PREPEND element -->
    <a href='#' data-include='path/to/file.html' data-include-type='replace'></a> <!-- will REPLACE element -->
    <a href='#' data-include='path/to/file.html' data-include-type='before'></a> <!-- will place new element BEFORE this element -->
    <a href='#' data-include='path/to/file.html' data-include-type='after'></a> <!-- will place new element AFTER this element -->

