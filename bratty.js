(function(global){

    var require_jQuery = function( callback ) {
        if ( typeof(jQuery) == 'undefined' ) {
            var script = document.createElement('script');
            script.src = 'http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js'; // latest 1.* version
            // consider http://code.jquery.com/jquery-latest.min.js

            var head = document.getElementsByTagName('head')[0]
                , done = false;

            script.onload = script.onreadystatechange = function() {		
                if ( !done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete') ) {
                    done = true;
                    script.onload = script.onreadystatechange = null;
                    head.removeChild(script);
                    head = script = null;

                    if ( typeof(jQuery) == 'undefined' )
                        throw new Error('Unable to load jQuery');
                    else
                        callback(jQuery);
                };
            };
            head.appendChild(script);
        } else
            callback(jQuery);
    };

    require_jQuery(function($) {

        var getGuid = function() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
                return v.toString(16);
            });
        },
        getFromHash = function( key ) {
            var results = location.hash.match( new RegExp('#' + key + '=(.*?)(#|$)') );
            return ( results && results[1] ? results[1] : null );
        };

        global.addEventListener('message', function(e) {
            // because "include" messages are always dispatched from the "top",
            // if a sub-page is loaded before it's dependencies it doesn't matter.  race conditions are not a concern.
            // the page will dispatch the event with the correct id from the top frame, who will then
            // do replacements, perhaps even in already externally loaded content (since it's by GUID).
            // just make sure if a page loads the content, the top then doesn't load the content again (by marking includes as fulfilled by removing their ids)
            var json = JSON.parse(e.data);
            if ( json.id && json.content ) {
                var method = json.include_type == 'prepend' ? 'prepend'
                            : json.include_type == 'before' ? 'before'
                            : json.include_type == 'after' ? 'after'
                            : json.include_type == 'replace' || json.include_type == 'replaceWith' ? 'replaceWith'
                            : 'append';
                $('[data-include-id=' + json.id + ']')[method]( json.content )
                    .removeAttr('data-include-id');
                // removing attribute means if this same id sends another message, element won't be appended twice.
                // its content from that id is already loaded.
            }
        }, false);

        $(function() {
            $('[data-include]').each( function(i,e) {
                var $this = $(this)
                    , id = getGuid()
                    , url = $this.data('include') + '#include-id=' + id;

                if ( $this.data('include-type') )
                    url += '#include-type=' + $this.data('include-type');

                $this.attr('data-include-id', id);
                $('<iframe>')
                    .css({
                        position: 'absolute'
                        , top: '-9999px'
                        , left: '-9999px'
                        , height: '0px'
                        , width: '0px'
                        , overflow: 'hidden'
                    })
                    .attr( 'src', url )
                    .appendTo( document.body );
            });

            var my_include_id = getFromHash('include-id');
            if ( my_include_id ) { // if my URL has an include-id, my parent is waiting for me...
                var els = $(window.document.childNodes).not('script').clone();
                els.find('script').remove(); // remove descendant script tags
                top.postMessage( JSON.stringify({
                    id: my_include_id
                    , content: els.html()
                    , include_type: getFromHash('include-type')
                }), '*' );
            }
        });
    });
})(this);